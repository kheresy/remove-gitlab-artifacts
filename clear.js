//Go to: https://gitlab.com/profile/personal_access_tokens
const API_KEY = "-----------------";

//You can find project id inside the "General project settings" tab
const PROJECT_ID = 17159201;
const PROJECT_URL = "https://gitlab.com/api/v4/projects/" + PROJECT_ID + "/"

const fetch = require('node-fetch');

let startpage = 0;
async function listAllJobs() {
	let jobs = [];
	for (let i = startpage, jobs = []; i == startpage || jobs.length > 0; i++) {
		jobs = await sendApiRequest(
			PROJECT_URL + "jobs/?per_page=50&page=" + (i + 1)
		).then(e => e.json());

		let cmdQueue = []
		for (let job of jobs) {
			if (job.artifacts.length > 0 && job.artifacts[0].file_format)
				console.log("Remove artifacts of job " + job.id );
				cmdQueue.push(sendApiRequest(
					PROJECT_URL + "jobs/" + job.id + "/artifacts",
					{ method: "DELETE" }
				));
		}

		Promise.all(cmdQueue).then(console.log(" - Finish page " + (i+1)));
	}
}

listAllJobs();

async function sendApiRequest(url, options = {}) {
	if (!options.headers)
		options.headers = {};
	options.headers["PRIVATE-TOKEN"] = API_KEY;

	return fetch(url, options);
}
